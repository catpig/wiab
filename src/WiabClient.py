#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib
from argparse import ArgumentParser
import sys
from time import sleep

#kivy
from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.lib import osc
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.utils import platform

#other 3rd party libs
from typing import Dict, List

kv = '''
<StartScreen>
	BoxLayout:
		Button:
			text: 'Make New Game'
			on_press: root.manager.current = 'new_game'
		Button:
			text: 'pong!'
			on_press: app.pong()

<NewGameScreen>
	GridLayout:
		cols: 2
		Button:
			text: 'New Game'
			on_press: app.start_new_game()
		Button:
			text: 'Back'
			on_press: root.manager.current = 'start'
		Label:
			text: 'Server'
		TextInput:
			id: server
			text: 'localhost'
			multiline: False
		Label:
			text: 'Port'
		TextInput:
			id: port
			text: '49000'
			multiline: False
		Label:
			text: 'No of Players'
		TextInput:
			id: player_count
			text: '4'
			multiline: False
'''

class StartScreen(Screen):
    pass

class NewGameScreen(Screen):
    #server = ObjectProperty()#None)
    pass

class WiabClient(App):
	def __init__(self):
		super(WiabClient, self).__init__()
		osc.init()
		socket = osc.listen('0.0.0.0', 49001)
		osc.bind(socket, self.on_osc, '/ping')
		Builder.load_string(kv)
		
		self.sm = ScreenManager()
		self.screen_start = StartScreen(name='start')
		self.sm.add_widget(self.screen_start)
		self.screen_new = NewGameScreen(name='new_game')
		self.sm.add_widget(self.screen_new)
	
	def start_new_game(self, *args):
		print("send", self.screen_new.ids.server.text)
		
		osc.sendMsg('/start_new_game', [1,"aäß"], ipAddr='127.0.0.1', port=49000)
	
	def pong(self, *args):
		print("send")
		osc.sendMsg('/pong', [1,"aäß"], ipAddr='127.0.0.1', port=49000)
	
	def on_osc(self, data, source):
		address, types, data = data
		print('Got: ' + str(data))
	
	def build(self):
		return self.sm

if __name__ == '__main__':
	WiabClient().run()
