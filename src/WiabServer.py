#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (C) Steffen Schaumburg 2016 and contributors, see docs/contributors.txt <steffen@schaumburger.info>
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
# In the original distribution you can find the license in docs/agpl_3.0.txt.

#python std lib
from argparse import ArgumentParser
import sys
from time import sleep

#kivy
from kivy.app import App
from kivy.base import EventLoop, runTouchApp
from kivy.clock import Clock
from kivy.config import Config
from kivy.event import EventDispatcher
from kivy.lib import osc
from kivy.properties import NumericProperty
from kivy.uix.widget import Widget

class WiabServer(EventDispatcher):
	def __init__(self):
		super(WiabServer, self).__init__()
		osc.init()
		socket = osc.listen('0.0.0.0', 49000)
		osc.bind(socket, self.start_new_game, '/start_new_game')
		osc.bind(socket, self.on_pong, '/pong')
		Clock.schedule_interval(lambda dt: osc.readQueue(socket), .1)
	
	def start_new_game(self, data, source):
		print("data", data, "source", source)
		address = data[0]
		typetags = data[1]
		data = data[2:]
		#for typetag in typetags:
	
	def on_pong(self, data, source):
		print("pong data", data, "source", source)
		address = data[0]
		typetags = data[1]
		data = data[2:]
		#for typetag in typetags:

if __name__ == '__main__':
	srv = WiabServer()
	EventLoop.add_event_listener(None)  # App won't run without one
	runTouchApp()
